
public class Main {
    public static void main(String[] args) {


        //int book_number = 0x21876;
        String book_number = Integer.toHexString(137334);
        System.out.printf(book_number + "\n");

        long tel = 89657776924L;
        System.out.printf(tel + "\n" );

        String last_two_num = Integer.toBinaryString((int)(tel%100));
        System.out.printf(last_two_num + "\n");

        String last_four_num = Integer.toOctalString((int)(tel%10000));
        System.out.printf(last_four_num + "\n");

        int num_mod = ((((Integer.parseInt(book_number,16))%100)-1)%26)+1;
        System.out.printf(num_mod + "\n");

        char sym = (char)(64+num_mod);
        System.out.printf("%c\n", sym);

    }


}